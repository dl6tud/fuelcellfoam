/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::mappingModel

Description
    Collects the parent-child mapping for all child meshes.

\*---------------------------------------------------------------------------*/

#include "mappingModel.H"


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::mappingModel::mappingModel
(
    const fvMesh& mesh, 
    const fvMesh& airMesh, 
    const fvMesh& fuelMesh, 
    const fvMesh& electrolyteMesh,
    const fvMesh& interconnectMesh,
    patchDatabase& pm
)
:
    mesh_(mesh),
    airMesh_(airMesh, pm),
    fuelMesh_(fuelMesh, pm),
    electrolyteMesh_(electrolyteMesh, pm),
    interconnectMesh_(interconnectMesh, pm),
    pm_(pm),
    electrolyteAnodeMap_(pm.electrolyteAnodePatch().size()),
    electrolyteCathodeMap_(pm.electrolyteCathodePatch().size())
 {
    // create a map from global faces to the anode surface on the electrolyte mesh
    forAll (electrolyteAnodeMap_, i)
    {
        electrolyteAnodeMap_[i] = electrolyteMesh_.faceMap()[pm_.electrolyteAnodePatch().start() + i];
    }

    // create a map from global faces to the cathode surface on the electrolyte mesh
    forAll (electrolyteCathodeMap_, i)
    {
        electrolyteCathodeMap_[i] = electrolyteMesh_.faceMap()[pm_.electrolyteCathodePatch().start() + i];
    }
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void Foam::mappingModel::clear(volScalarField& field)
{
    scalarField& fieldIn = field.primitiveFieldRef();
    fieldIn = 0.0;
}


void Foam::mappingModel::clear(surfaceScalarField& field)
{
    scalarField& fieldIn = field.primitiveFieldRef();
    fieldIn = 0.0;
}


void Foam::mappingModel::mapFromCell
(
    const volScalarField& globalField,
    volScalarField& airField,
    volScalarField& fuelField,
    volScalarField& electrolyteField,
    volScalarField& interconnectField
)
{
    {
        forAll(airField, cellI)
        {
            airField[cellI] = globalField[airMesh_.cellMap()[cellI]];
        }
        airField.correctBoundaryConditions();
    }

    {
        forAll(fuelField, cellI)
        {
            fuelField[cellI] = globalField[fuelMesh_.cellMap()[cellI]];
        }
        fuelField.correctBoundaryConditions();
    }

    {
        forAll(electrolyteField, cellI)
        {
            electrolyteField[cellI] = 
		globalField[electrolyteMesh_.cellMap()[cellI]];
        }
        electrolyteField.correctBoundaryConditions();
    }

    {
        forAll(interconnectField, cellI)
        {
            interconnectField[cellI] = 
		globalField[interconnectMesh_.cellMap()[cellI]];
        }
        interconnectField.correctBoundaryConditions();
    }
}

// ************************************************************************* //
