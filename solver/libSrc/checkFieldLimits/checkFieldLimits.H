/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::checkFieldLimits

Description
    Computes the min, mean and max of a field and provides a warning, if 
    certain limites are exceeded. Helpful to find the first problematic 
    equation / field for a diverging simulation - because there the first
    warning appears.
    Checks also, if relaxation values have been given (or have been forgotten).

\*---------------------------------------------------------------------------*/

#ifndef checkFieldLimits_H
#define checkFieldLimits_H

#include "scalar.H"
#include "volFields.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                  Class checkFieldLimits Declaration
\*---------------------------------------------------------------------------*/

class checkFieldLimits {

    // Private member functions

        //- Disallow copy construct
        checkFieldLimits(const checkFieldLimits&) = delete;

        //- Disallow default bitwise assignment
        void operator=(const checkFieldLimits&) = delete;

public:

    // Constructors

        checkFieldLimits(){};

    // Destructor

        ~checkFieldLimits(){};


    // Member Functions

	//- Checks if field values are between min and max.
	// Optionally, the boundary values are included (if true).
    	bool check(const volScalarField&, scalar min, scalar max, bool boundary=true) const;
        bool check(const volVectorField&, scalar min, scalar max, bool boundary=true) const;
        bool check(word, const scalarField&, scalar min, scalar max) const;
	//- Limits the field to min and max
        bool limit(volScalarField&, scalar min, scalar max, bool boundary=true) const;
	//- Checks, if relaxation values for the specific field have been provided.
	bool checkRelax(const volScalarField&) const;
        //- Checks, if relaxation values for the specific field have been provided.
        bool checkRelax(const volVectorField&) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
