#!/bin/bash
# writes the git version into the solver

dir=`pwd`
cd ../
hash=`git rev-parse HEAD`
date=`date`
cd $dir

echo "Info << \"Git: "$hash"\" << nl" > gitVersion.H
echo "     << \"     "$date "\"<< nl" >> gitVersion.H
echo "     << \"// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\"" >> gitVersion.H
echo "     << endl;" >> gitVersion.H
