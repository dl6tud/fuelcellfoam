/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::electrochemistryModel

Description
    Solves the electrochemistry including boundary conditions for consumed
    and produced species, voltage and current density.

\*---------------------------------------------------------------------------*/

#ifndef electrochemistryModel_H
#define electrochemistryModel_H

#include "fvMesh.H"
#include "patchDatabase.H"
#include "checkFieldLimits.H"
#include "materialDatabase.H"
#include "ohmicOverpotentialModel.H"
#include "activationOverpotentialModel.H"
#include "electrolyteModel.H"
#include "mappingModel.H"
#include "flowBalance.H"
#include "PtrList.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                  Class electrochemistryModel Declaration
\*---------------------------------------------------------------------------*/

class electrochemistryModel
:
    private checkFieldLimits
{
private:

    // Private data

	//- Faraday constant
   	const scalar F_ = physicalConstant::F.value();         // C/mol
	//- Faraday constant
    	const scalar FF_ = physicalConstant::FF.value();       // C/kmol
	//- Universal gas constant
    	const scalar Rgas_ = physicalConstant::Rgas.value();   // J/(mol K)
	//- Temperature at standard condition
    	const scalar Tr_ = 298.15;
	//- Reference to time
    	const Time& runTime_;

    	const fvMesh& electrolyteMesh_;
    	const fvMesh& airMesh_;
    	const fvMesh& fuelMesh_;

	//- Saves patch names and IDs
    	patchDatabase& pm_;
	//- Properties of air
    	materialDatabase& matAir_;
	//- Properties of fuel
   	 materialDatabase& matFuel_;
	//- Computes mass fluxes at different patches
    	flowBalance& airBalance_;
	//- Computes mass fluxes at different patches
    	flowBalance& fuelBalance_;
	//- Solves water transport through, and content in membrane
    	electrolyteModel& elec_;
    	const ohmicOverpotentialModel& ohm_;
    	const activationOverpotentialModel& airActivationOverpotential_;
    	const activationOverpotentialModel& fuelActivationOverpotential_;

	//- Mapping between parent and child meshes
   	 mappingModel& map_;

	//- Dictionary with information on electrochemistry
    	IOdictionary rxnDict_;
	//- Stoichiometric coefficients of species
    	HashTable<scalar,word> rxnSpCoef_;

	//- Molar flux of consumed fuel species
    	double fluxMolarMass_;
	//- Electron flux
    	double fluxElectrons_;
	//- Exponent accounting for blocking due to liquid water
	//- as (1 - s)^activationFactor
    	scalar activationFactor_;

	//- During pre-iteration, activation overpotential is neglected
    	scalar PreIterAct_;
	//- Relaxation factor for current
    	scalar relaxCurrent_;
	//- Relaxation factor for water transport through membrane
    	scalar relaxMem_;
	//- Thickness of membrane (number of cells)
    	scalar numCells_;
    	//- Direction of the current (positive charge) in electrolyte
    	// minus: anode -> cathode
    	// plus: cathode -> anode
    	scalar currentSign_;
	//- Voltage correction relaxation coefficient for galvanostatic run
    	scalar Rhat_;
        //- Scalar cell voltage
        dimensionedScalar V_;
	//- Mean current density
    	dimensionedScalar ibar0_;

	//- Galvanostatic or potentiostatic run
    	Switch galvanostatic_;
	//- First iteration of solver
    	bool firstTime_;

	//- Current density (relaxed)
    	scalarField i_;
	//- Current density of last iteration
    	scalarField iOld_;
	//- Current density (new, not relaxed)
    	scalarField iNew_;
	//- Anode temperature
    	scalarField anodeT_;
	//- Cathode temperature
    	scalarField cathodeT_;
	//- Nernst potential (open-circuit) at patch
    	scalarField Nernst_;
	//- Old mass flux
    	scalarField mfluxOld_;
	//- Molar fraction at patch
    	PtrList<scalarField> xAir_;
	//- Molar fraction at patch
    	PtrList<scalarField> xFuel_;

	//- Cell voltage (field)
    	volScalarField voltage_;
	//- Nernst potential (as field, inside electrolyte)
    	volScalarField NernstPot_;
	//- Anode activation overpotential
    	volScalarField etaA_;
	//- Cathode activation overpotential
    	volScalarField etaC_;
	//- Current density (inside electrolyte)
    	volScalarField idensity_;
	//- Heat released due to reaction
    	volScalarField electrochemicalHeating_;

	//- Cell temperature
    	const volScalarField& Tcell_;

	//- Air velocity
    	volVectorField& Uair_;
	//- Fuel velocity
    	volVectorField& Ufuel_;

	//- Species mass fraction
    	PtrList<volScalarField>& Yair_;
	//- Species mass fraction
    	PtrList<volScalarField>& Yfuel_;

    // Private member functions

        //- Disallow copy construct
        electrochemistryModel(const electrochemistryModel&) = delete;

        //- Disallow default bitwise assignment
        void operator=(const electrochemistryModel&) = delete;

	//- Interpolate global temperture to anode/cathode patch
    	void getTemperature();
	//- Compute mole fractions at patches
    	void calcMoleFractions();
	//- Get Nernst potential
    	void calcNernst();
	//- Compute the current density
    	void calcCurrentDensity();
	//- Correct voltage to fit to target mean current density
    	void correctVoltage();
	//- Compute electrochemical heat release 
    	void calcElectrochemicalHeating();
	//- Update mass flux boundary conditions
    	void updateBoundaryConditions();
	//- Compute activation overpotentials
   	void calcActivationOverpotential();

public:

    // Constructors

        electrochemistryModel
        (
	    patchDatabase&,
	    materialDatabase&,
	    materialDatabase&,
	    flowBalance&,
	    flowBalance&,
	    electrolyteModel&,
  	    const ohmicOverpotentialModel&,
	    const activationOverpotentialModel&,
	    const activationOverpotentialModel&,
	    mappingModel&,
	    IOdictionary&,
	    volScalarField&
	);

    // Destructor

        ~electrochemistryModel(){};


    // Member Functions

	//- Solve electrochemistry
    	void solve();

	//- Return reference to electrochemical heat
    	const volScalarField& electrochemicalHeating()
    	{
	    return electrochemicalHeating_;
    	}
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
