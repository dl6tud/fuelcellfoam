/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::solidZone

Description
    List container for solid zones. Provides the properties of the solid
    zones.

\*---------------------------------------------------------------------------*/

#ifndef solidZoneList_H
#define solidZoneList_H

#include "fvMesh.H"
#include "dictionary.H"
#include "fvMatricesFwd.H"
#include "solidZone.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// Forward declaration of friend functions and operators
class solidZoneList;
Ostream& operator<<(Ostream& os, const solidZoneList& models);

/*---------------------------------------------------------------------------*\
                      Class solidZoneList Declaration
\*---------------------------------------------------------------------------*/

class solidZoneList
:
    public PtrList<solidZone>
{
private:

    // Private Member Functions

        //- Disallow default bitwise copy construct
        solidZoneList(const solidZoneList&);

        //- Disallow default bitwise assignment
        void operator=(const solidZoneList&);


protected:

    // Protected data

        //- Reference to the mesh
        const fvMesh& mesh_;


public:

    //- Constructor
    solidZoneList(const fvMesh& mesh, const dictionary& dict);

    //- Destructor
    ~solidZoneList();


    // Member Functions

        //- Reset the source list
        void reset(const dictionary& dict);

        // I-O

            //- Read dictionary
            bool read(const dictionary& dict);

            //- Write data to Ostream
            bool writeData(Ostream& os) const;

            //- Ostream operator
            friend Ostream& operator<<
            (
                Ostream& os,
                const solidZoneList& models
            );
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
