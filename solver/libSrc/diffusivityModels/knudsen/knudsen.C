/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "knudsen.H"
#include "addToRunTimeSelectionTable.H"

#include "volFields.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace diffusivityModels
{
 
defineTypeNameAndDebug(knudsen, 0);
addToRunTimeSelectionTable(diffusivityModel, knudsen, dictionary);


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

knudsen::knudsen
(
    const fvMesh& mesh,
    scalarField& diff,
    const labelList& cells,
    const dictionary& dict
)
:
    diffusivityModel(mesh, diff, cells, dict),
    Tname_(dict_.lookup("Tname")),
    dPore_("dPore", dimLength, dict_),
    MW_("MW", dimensionSet(1,0,0,0,-1,0,0), dict_)
{}


knudsen::knudsen
(
    const fvMesh& mesh,
    scalarField& diff,
    const labelList& cells,
    word Tname,
    const dimensionedScalar& dPore,
    const dimensionedScalar& MW
)
:
    diffusivityModel(mesh, diff, cells),
    Tname_(Tname),
    dPore_(dPore),
    MW_(MW)
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void knudsen::writeData()
{
    Info<< "    diffusivityModels::knudsen:" << nl
        << "        dPore = " << dPore_ << nl
        << "        MW = " << MW_ << endl;
}


void knudsen::evaluate()
{
    //  D_{knudsen} = (poreDiameter/2)*97*sqrt(T/MW)
    //  where
    //      poreDiameter = (m)
    //      T ............ (K)
    //      MW ........... (kg/mol)
    //  Geankoplis, Christie J, Transport Processes and Unit Operations,
    //  second edition (1983), Allyn and Bacon Series in Engineering,
    //  ISBN 0-205-07788-9, page 452.

    const volScalarField& T =
        mesh_.thisDb().lookupObject<volScalarField>(Tname_);

    forAll(cells_, i)
    {
        diff_[cells_[i]] =
            48.5*dPore_.value() // 1e3 to convert kg/mol to kg/kmol
            *Foam::sqrt(T.internalField()[cells_[i]]/(1e3*MW_.value()));
    }
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace diffusivityModels
} // End namespace Foam

// ************************************************************************* //
