/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::ohmicOverpotentialModel

Description
    Area specific resistance computed according to springer model.

    Reference:
    \verbatim
        Springer, T. E., Zawodinski, T. A., Gottesfeld, S. (1991)
        Polymer Electrolyte Fuel Cell Model.
        J. Electrochem. Soc. 138(8), 2334-2342.
    \endverbatim

\*---------------------------------------------------------------------------*/

#include "springer.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"


// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(springer, 0);
    addToRunTimeSelectionTable(ohmicOverpotentialModel, springer, dictionary);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::springer::springer
(
    const fvMesh& mesh,
    const patchDatabase& pm,
    const dictionary& dict
)
:
    ohmicOverpotentialModel(mesh, pm, dict),
    correctSigma_(dict.lookupOrDefault<scalar>("correctSigma", 1.0)),
    contactResistance_(dict.lookupOrDefault<dimensionedScalar>("contactResistance", 0.0))
{
    if (contactResistance_.value() == 0)
    {
        Info<< "    Contact resistance: 0 Warning!" << endl;
    }
    else
    {
        Info<< "    Contact resistance: " << contactResistance_.value() << endl;
    }
    Info<< "    Correction factor conductivity: " << correctSigma_ << endl;
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

Foam::tmp<Foam::scalarField> Foam::springer::ASR
(
     const scalarField& anodeT,
     const scalarField& lambda
) const
{
    tmp<scalarField> pASR
    (
        new scalarField (anodeT.size(), 1.0)
    );
    scalarField& ASR = pASR.ref();

    // anodeT corresponds to electrolyteAnodePatch, and ASR, too ... but lambda not!
    const labelUList& aOwner = mesh().boundary()[pm().electrolyteAnodeID()].faceCells(); // owner of patch faces
    forAll(anodeT, facei)
    {
	// formula (25)
	// conductivity from S/cm to S/m is factor 100
	// ASR = electrolyte thickness / conductivity
        if(lambda[aOwner[facei]] < 1.0) // if water content < 1, assume it is exactly 1
        {
            ASR[facei] = hE().value()/
		(correctSigma_*Foam::exp(1268.*(1./303. - 1./anodeT[facei]))
		*(0.5139 - 0.326));
        }
        else
        {
            ASR[facei] = hE().value()/
		(correctSigma_*Foam::exp(1268.*(1./303. - 1./anodeT[facei]))
		*(0.5139*lambda[aOwner[facei]] - 0.326));
        }
    }

    
    if (R().value() > 0.0)
    {
	bool limited = false;

	scalar minASR = R().value()*(1.0 - deviation());
	scalar maxASR = R().value()*(1.0 + deviation());

	forAll(anodeT, facei)
	{
	    if (ASR[facei] < minASR)
	    {
		ASR[facei] = minASR;
		limited = true;
	    }
	    else if (ASR[facei] > maxASR)
	    {
		ASR[facei] = maxASR;
		limited = true;
	    }
	}
	if (limited)
	{
	    Info << "Warning: Limited ASR" << endl;
	}
    }

    check("ASR", ASR, 0, GREAT);

    ASR += contactResistance_.value();

    return pASR;
}

// ************************************************************************* //
