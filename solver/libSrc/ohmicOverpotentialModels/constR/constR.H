/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::ohmicOverpotentialModel

Description
    Constant area specific resistance (ASR) - read from file.

\*---------------------------------------------------------------------------*/

#ifndef constR_H
#define constR_H

#include "ohmicOverpotentialModel.H"
#include "checkFieldLimits.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                           Class constR Declaration
\*---------------------------------------------------------------------------*/

class constR
:
    public ohmicOverpotentialModel
{

    // Private member functions

        //- Disallow copy construct
        constR(const constR&);

        //- Disallow default bitwise assignment
        void operator=(const constR&);


public:

    //- Runtime type information
    TypeName("constR");


    // Constructors

        constR
        (
            const fvMesh& mesh,
            const patchDatabase& pm,
            const dictionary& dict
        );


    // Destructor

        ~constR()
        {}


    // Member functions

        //- Returns area specific resistance
        virtual tmp<scalarField> ASR
        (
            const scalarField& T,
            const scalarField& lambda
        ) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //

