/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::electrolyteModel

Description
    Computes water content in, and water transfer through membrane, and 
    provies membrane properties.

\*---------------------------------------------------------------------------*/

#include "electrolyteModel.H"


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::electrolyteModel::electrolyteModel
(
    const fvMesh& electrolyteMesh,
    materialDatabase& matAir,
    materialDatabase& matFuel,
    patchDatabase& pm,
    const ohmicOverpotentialModel& ohm
)
:
    IOdictionary
    (
        IOobject
        (
            "electrolyteProperties",
            electrolyteMesh.time().constant(),
            electrolyteMesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    ),
    mesh_(electrolyteMesh),
    rho_("rho", dimDensity, *this),
    cp_("cp", dimensionSet(0,2,-2,-1,0,0,0), *this),
    k_("k", dimensionSet(1,1,-3,-1,0,0,0), *this),
    Meq_("Meq", dimensionSet(1,0,0,0,-1,0,0), *this),
    T0_(dimensionedScalar("T0", dimTemperature, 273.15)),
    corr_(lookupOrDefault<scalar>("corrector", 1.0)),
    hE_(ohm.hE()),
    matAir_(matAir),
    matFuel_(matFuel),
    pm_(pm),
    lambda_
    (
        IOobject
        (
            "lambda",
            electrolyteMesh.time().timeName(),
            electrolyteMesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        electrolyteMesh,
        dimensionedScalar("0", dimensionSet(0,0,0,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    Q_
    (
        IOobject
        (
            "Q",
            electrolyteMesh.time().timeName(),
            electrolyteMesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        electrolyteMesh,
        dimensionedScalar("Q", dimensionSet(1,-2,-1,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    Qold_
    (
        IOobject
        (
            "Qold",
            electrolyteMesh.time().timeName(),
            electrolyteMesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        electrolyteMesh,
        dimensionedScalar("Q", dimensionSet(1,-2,-1,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    D_
    (
        IOobject
        (
            "D",
            electrolyteMesh.time().timeName(),
            electrolyteMesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        electrolyteMesh,
        dimensionedScalar("D", dimensionSet(0,2,-1,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    gradLambda_
    (
        IOobject
        (
            "gradLambda",
            electrolyteMesh.time().timeName(),
            electrolyteMesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        electrolyteMesh,
        dimensionedScalar("0", dimensionSet(0,-1,0,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    T_
    (
        IOobject
        (
            "T",
            electrolyteMesh.time().timeName(),
            electrolyteMesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        electrolyteMesh,
        dimensionedScalar("T", dimensionSet(0,0,0,1,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    )
{
    // echo inputs from dictionary to stdout
    Info<< nl << "Creating electrolyte model" << nl
        << "    rhoElectrolyte  = " << rho_  << nl
        << "    cpElectrolyte   = " << cp_   << nl
        << "    kElectrolyte    = " << k_    << nl
        << endl;
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void Foam::electrolyteModel::solveWaterContent()
{
    // get saturation pressure
    scalarField waterAir(pm_.cathodePatch().size());

    // air temperature in °C
    scalarField Ta
    ( 
	matAir_.T().boundaryField()[pm_.cathodeID()] - T0_.value()
    );
    scalarField pSatAirp
    (
	101325.*pow(10, -2.1794 + 0.02953*Ta - 9.1837e-5*Ta*Ta + 1.4454e-7*Ta*Ta*Ta)
    );
    
    // fuel temperature in °C
    scalarField Tf
    (
	matFuel_.T().boundaryField()[pm_.anodeID()] - T0_.value()
    );
    scalarField pSatFuelp
    ( 
	101325.*pow(10, -2.1794 + 0.02953*Tf - 9.1837e-5*Tf*Tf + 1.4454e-7*Tf*Tf*Tf)
    );

    // get water activity air
    scalarField aAir
    ( 
	matAir_.X()[matAir_.waterIndex()].boundaryField()[pm_.cathodeID()]
	*matAir_.p().boundaryField()[pm_.cathodeID()]/pSatAirp
	+ 2*matAir_.s().boundaryField()[pm_.cathodeID()]
    );
   
    // get water activity fuel
    scalarField aFuel
    (
	matFuel_.X()[matFuel_.waterIndex()].boundaryField()[pm_.anodeID()]
	*matFuel_.p().boundaryField()[pm_.anodeID()]/pSatFuelp 
	+ 2*matFuel_.s().boundaryField()[pm_.anodeID()]
    );

    // limit activity to an interval 0 ... 3
    forAll(aAir, i)
    {
	if (aAir[i] > 3){aAir[i] = 3;}
	else if (aAir[i] < 0){aAir[i] = 0;}
    }
    forAll(aFuel, i)
    {
	if (aFuel[i] > 3){aFuel[i] = 3;}
	else if (aFuel[i] < 0){aFuel[i] = 0;}
    }

    // get water content at patch cathodeID in air (air_to_electrolyte)
    scalarField lambdaAir
    (
	0.043 + 17.81*aAir - 39.85*aAir*aAir + 36.*aAir*aAir*aAir
    );

    forAll(aAir, i) // alternative function if activity larger than 1
    {
	if(aAir[i] > 1){lambdaAir[i] = 14. + 1.4*(aAir[i] - 1.);}
    }

    // water content at patch anodeID in fuel (fuel_to_electrolyte)
    scalarField lambdaFuel
    (
	0.043 + 17.81*aFuel - 39.85*aFuel*aFuel + 36.*aFuel*aFuel*aFuel
    );

    forAll(aFuel, i)
    {
        if(aFuel[i] > 1){lambdaFuel[i] = 14. + 1.4*(aFuel[i] - 1.);}
    }

    // get water content in membrane
    lambda_.boundaryFieldRef()[pm_.electrolyteCathodeID()] = 
	pm_.airCathodeToElectrolyteCathode().faceInterpolate(lambdaAir);
    lambda_.boundaryFieldRef()[pm_.electrolyteAnodeID()] = 
	pm_.fuelAnodeToElectrolyteAnode().faceInterpolate(lambdaFuel);

    // copy values from one patch to the cell centre
    const fvPatchScalarField& cLambda = 
	lambda_.boundaryField()[pm_.electrolyteCathodeID()];
    const labelUList& cOwner = // owner of patch faces
	mesh_.boundary()[pm_.electrolyteCathodeID()].faceCells();

    forAll(cLambda, facei)
    {
	lambda_[cOwner[facei]] = cLambda[facei];
    }

    // compute mean value and gradient of lambda
    const fvPatchScalarField& aLambda = // anodeLambda
	lambda_.boundaryField()[pm_.electrolyteAnodeID()];
    const labelUList& aOwner = // owner of patch faces
	mesh_.boundary()[pm_.electrolyteAnodeID()].faceCells();

    forAll(aLambda, facei)
    {
        gradLambda_ [aOwner[facei]] = // dLambda / dz
	    (lambda_[aOwner[facei]] - aLambda[facei])/hE_.value();
        // take cell centre values and face values of other patch to compute mean
        lambda_[aOwner[facei]] = 
	    (aLambda[facei] + lambda_[aOwner[facei]])/2.;
    }
    lambda_.correctBoundaryConditions();

    check(gradLambda_, -GREAT, GREAT,false);
    check(lambda_, -GREAT, GREAT,false);
}

void Foam::electrolyteModel::solveWaterFlux(const volScalarField& idensity)
{
    Info<< nl << "Computing water flux through membrane" << endl;

    // compute diffusivity of water in membrane
    forAll(D_, i) // H2 in F: 1e-13; O2 in H20: 1e-9; Li in Bi 1e-8; here: 1e-12 OK
    {
        if (lambda_[i] < 3)
        {
	    D_[i] = 3.1e-7*lambda_[i]*(Foam::exp(0.28*lambda_[i]) - 1.)
		*Foam::exp(-2436./T_[i]);
        }
        else
        {
            D_[i] = 4.17e-8*lambda_[i]*(161.*Foam::exp(-lambda_[i]) + 1.)
		*Foam::exp(-2436./T_[i]);
        }
    }

    // molar mass water
    dimensionedScalar Mw("Mw",dimensionSet(1,0,0,0,-1,0,0),0.018);

    // H+ flux is twice the H2 flux N_H2 = J /(zF)
    volScalarField Hflux(2*idensity/(2*physicalConstant::FF.value()));

    // compute water flux through membrane
    // we take anode to cathode, i.e. the path of H+ as positive
    volScalarField Qdrag
    ( 
	Mw*dimensionedScalar("drag", dimensionSet(0,0,-1,0,1,-1,0) ,2.5)
	*lambda_*Hflux/22. // 2e-7
    );
    volScalarField Qdiff(-Mw*rho_*D_*gradLambda_/Meq_); // 4e-3
    Qold_ = Q_;
    Q_ = Qdrag + Qdiff;

    Q_ *= corr_; // can be set to zero

    Q_.correctBoundaryConditions(); // we need later the value at the boundary

    Info<< "mean water flux anode to cathode (drag, diffusion, total): "
        << Foam::gAverage(Qdrag) <<" , "
        << Foam::gAverage(Qdiff) <<" , "
        << Foam::gAverage(Q_) << endl;
}

// ************************************************************************* //
