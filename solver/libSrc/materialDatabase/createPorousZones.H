/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::materialDatabase

Description
    Provides material properties of a gas.

\*---------------------------------------------------------------------------*/

{
    // create contact angle, porosity, permeability fields
    Info<< "Creating " << zName_ << " porous zones" << endl;

    Info<< "    number of porous zones = " << porousZones_.size() << nl;
    forAll(porousZones_, i)
    {
        label iz = mesh_.cellZones().findZoneID(porousZones_[i].zoneName());
        Info<< "    zone " << porousZones_[i].cellZoneIDs()
            << " " << porousZones_[i].zoneName()
            << " : size = " << mesh_.cellZones()[iz].size()
            << "; k = " << porousZones_[i].k().value()
            << "; theta = " << porousZones_[i].theta().value()*180./constant::mathematical::pi
            << "; porosity = " << porousZones_[i].porosity() << nl;
    }

    // artificial permeability of the gas channels
    // if too high, water diffusion coefficent becomes too high and explodes
    dimensionedScalar globalPermeability
	("permeability", dimensionSet(0,2,0,0,0,0), speciesDict_);

    // artifical contact angle of the channel
    dimensionedScalar globalTheta("theta", dimless, speciesDict_);
    // radiands
    globalTheta *= (constant::mathematical::pi/180.);

    // get permeability and theta as fields
    scalarField& fK0 = K0_.primitiveFieldRef();
    scalarField& ftheta = theta_.primitiveFieldRef();

    fK0 = globalPermeability.value();
    ftheta = globalTheta.value();

    // fill the theta, epsilon, permeability field
    forAll(porousZones_, iz)
    {
        label znId =
            mesh_.cellZones().findZoneID(porousZones_[iz].zoneName());
        labelList znCells(mesh_.cellZones()[znId]);

        dimensionedScalar thetaZn = porousZones_[iz].theta();
        scalar epsilonZn = porousZones_[iz].porosity();
        dimensionedScalar permeabilityZn = porousZones_[iz].permeability();

        forAll(znCells, cellI)
        {
            theta_[znCells[cellI]] = thetaZn.value();
            epsilon0_[znCells[cellI]] = epsilonZn;
            K0_[znCells[cellI]] = permeabilityZn.value();
            porous_[znCells[cellI]] = 1; // mark porous cells
        }
    }
    theta_.correctBoundaryConditions();
    epsilon0_.correctBoundaryConditions();
    K0_.correctBoundaryConditions();
    porous_.correctBoundaryConditions();
}

// ************************************************************************* //
