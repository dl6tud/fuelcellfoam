/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::materialDatabase

Description
    Provides material properties of a gas.

\*---------------------------------------------------------------------------*/

#include "materialDatabase.H"


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::materialDatabase::materialDatabase
(
    PtrList<volScalarField>& Y,
    const volScalarField& T,
    volVectorField& U,
    const volScalarField& p,
    volScalarField& s
)
:
    mesh_(T.mesh()),
    zName_(mesh_.name()),
    speciesDict_
    (
    	IOdictionary
    	(
            IOobject
            (
            	"phaseProperties",
            	mesh_.time().constant(),
            	mesh_,
            	IOobject::MUST_READ,
            	IOobject::NO_WRITE
            )
    	)
    ),
    speciesNames_(speciesDict_.lookup("species")),
    porousZones_
    (
	mesh_,    
	IOdictionary
    	(
            IOobject
            (
                "porousZones",
                mesh_.time().constant(),
                mesh_,
                IOobject::MUST_READ,
                IOobject::NO_WRITE
            )
        )
    ),
    Y_(Y),
    T_(T),
    U_(U),
    p_(p),
    s_(s),
    X_(speciesNames_.size()),
    molarCp_(speciesNames_.size()),
    muS_(speciesNames_.size()),
    kS_(speciesNames_.size()),
    diffSp_(speciesNames_.size()),
    k_
    (
        IOobject
        (
            "k",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("k", dimensionSet(1,1,-3,-1,0,0,0), 0.0),
        zeroGradientFvPatchScalarField::typeName
    ),
    mu_
    (
        IOobject
        (
            "mu",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("mu", dimensionSet(1,-1,-1,0,0,0,0), 0.0),
        zeroGradientFvPatchScalarField::typeName
    ),
    nu_
    (
        IOobject
        (
            "nu",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("nu", dimensionSet(0,2,-1,0,0,0,0), 0.0),
        zeroGradientFvPatchScalarField::typeName
    ),
    rho_
    (
        IOobject
        (
            "rho",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("rho", dimensionSet(1,-3,0,0,0,0,0), 0.0),
        zeroGradientFvPatchScalarField::typeName
    ),
    cp_
    (
        IOobject
        (
            "cp",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("cp", dimensionSet(0,2,-2,-1,0,0,0), 0.0),
        zeroGradientFvPatchScalarField::typeName
    ),
    cm_
    (
        IOobject
        (
            "cm",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("cp", dimensionSet(1,2,-2,-1,-1,0,0), 0.0),
        zeroGradientFvPatchScalarField::typeName
    ),
    theta_
    (
        IOobject
        (
            "theta",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("theta", dimless, 0.0),
        zeroGradientFvPatchScalarField::typeName
    ),
    epsilon0_
    (
        IOobject
        (
            "epsilon0",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("epsilon", dimless, 1.0),
        zeroGradientFvPatchScalarField::typeName
    ),
    porous_
    (
        IOobject
        (
            "porous",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("porous", dimless, 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    K0_
    (
        IOobject
        (
            "K0",
            mesh_.time().timeName(),
            mesh_,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("K0", dimensionSet(0,2,0,0,0,0,0), 1.0),
        zeroGradientFvPatchScalarField::typeName
    ),
    H2O_(-1),
    inertSpecie_(speciesDict_.lookup("inertSpecie")),
    diff_(mesh_.nCells()),
    species_(speciesNames_.size()),
    znCells_(porousZones_.size()),
    znDiffDict_(porousZones_.size()),
    diffModels_(1 + porousZones_.size()),
    sumYIonMI_("YoM", 0*Y_[0]),
    expBlockage_(speciesDict_.lookupOrDefault<scalar>("expWaterBlockage", 1.5))
{
    Info<< "Creating material properties for phase " << zName_ << endl;

    #include "createSpecies.H"
    #include "createMaterials.H"
    #include "createPorousZones.H"
    #include "createDiffusivityModels.H"

    Info<< "Using a pore blockage due to water coefficient of " 
	<< expBlockage_ << endl;
}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::materialDatabase::~materialDatabase()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

// update all material properties: rho, mu, nu, k, cp, diffusivity
void materialDatabase::updateMaterialProperties()
{
    Info<< nl << "Updating material properties of phase " << zName_ << endl;

    // compute density
    {
        // rho = (p/RT)*Sum(xi*Mi);     xi = (yi/Mi)/Sum(yj/Mj)
        //                           xi*Mi = (yi)/Sum(yj/Mj)
        //                      Sum(xi*Mi) = Sum_i(yi/Sum_j(yj/Mj)
        //                                 = Sum_i(yi)/Sum_j(yj/Mj))
        //                                 = 1/Sum_j(yj/Mj)
        // rho = (p/RT)/Sum(yj/Mj)

        const scalar RR = physicalConstant::RR.value();

        sumYIonMI_ = 0.0;
        forAll(species_, i)
        {
            sumYIonMI_ += Y_[i]/species_[i].MW();
        }

        scalarField& rho = rho_.primitiveFieldRef();
        rho = p_/(RR*T_)/sumYIonMI_;
        rho_.correctBoundaryConditions();

	check(rho_, SMALL, GREAT, false);
	// density should not get negative or zero
    	limit(SMALL, rho_, "rho");
    }

    // obtain mu (mixture) from mu (species components)
    {
        scalarField& mu = mu_.primitiveFieldRef();
        mu = 0;

        // initially using a simple linear combination weighted by mass fraction
        forAll(species_, I)
        {
            scalarField muI(muS_[I].polyVal(T_.primitiveField()));
            mu += muI*Y_[I].primitiveField();
        }
        mu *= 1e-7;    // convert from micro-Poise to kg/(m s)
        mu_.correctBoundaryConditions();

	check(mu_, SMALL, GREAT, false);
	// mu should not get negative or zero
        limit(SMALL, mu_, "mu");
    }

    // obtain nu (mixture) from mu (mixture) and rho (mixture)
    {
        scalarField& nu = nu_.primitiveFieldRef();
        nu = 0;
        nu = mu()/rho();
        nu_.correctBoundaryConditions();

	check(nu_, SMALL, GREAT,false);
    }

    // obtain k (mixture) from k (species components)
    {
        scalarField& kAIn = k_.primitiveFieldRef();
        kAIn = 0;

        // initially using a simple linear combination weighted by mass fraction
        forAll(species_, I)
        {
            scalarField kI(kS_[I].polyVal(T_.primitiveField()));
            kAIn += kI*Y_[I].primitiveField();
        }
        kAIn *= 1e-2; // k = 0.01*(ToddYoung polynomial value)
                      // Table 3, J. Power Sources 110 (2002) 186-200 (page 190)

        k_.correctBoundaryConditions();

	check(k_, SMALL, GREAT, false);
	// k should not be negative or zero
        limit(SMALL, k_, "k");
    }


    // c_s = c_m / M
    // obtain cp (mixture) from cp (species components)
    {
        scalarField& cp = cp_.primitiveFieldRef();
        scalarField& cm = cm_.primitiveFieldRef();

        cp = 0; cm = 0;

        // initially using a simple linear combination weighted by mass fraction
 
        forAll(species_, I)
        {
	    scalar rMWi = 1.0/(species()[I].MW()*1e-3);    // 1/(kg/mol)

            scalarField cmI(molarCp_[I].polyVal(T_.primitiveField()));
	    scalarField cpI(cmI * rMWi);

            cp += cpI*Y_[I].primitiveField();
            cm += cmI*Y_[I].primitiveField();

        }
        cp_.correctBoundaryConditions();
        cm_.correctBoundaryConditions();

	check(cp_, SMALL, GREAT, false);
        check(cm_, SMALL, GREAT, false);

	// should not become negative or zero
        limit(SMALL, cp_, "cp");
        limit(SMALL, cm_, "cm");
    }


    // Compute diffusivities
    {

        // For a fixed-diffusivity model we only need to assign the diffusivity
        // Otherwise we must use a binary model, possibly modified for porosity
        // For a binary model we calculate pwise diffusivities D[a,b],
        // and calculate species diffusivity in mixture as [Wilke 1950]
        //
        //                   1 - x[a]
        //         D[a] = ---------------------- , where x[a] is mole fraction of specie a
        //            sum_{b!=a}(x[b]/D[a,b]
        //
        // We consider "a" as the diffusing specie and "b" as the background specie --
        // this is only relavent to binary porous models invoking knudsen diffusion.
        //

        // The non-porous zone is automatically written first!

        forAll(species_, a)
        {
            diff_ = 0;

            // diffSp[a] is diffusivity field for specie a, to be used in YEqn
            scalarField& diffSpIn = diffSp_[a].primitiveFieldRef();
            diffSpIn = 0;

            if(species_[a].name() != inertSpecie_)
            {
                Info<< nl << zName_ << "Species " << species_[a].name() << nl;
    
                forAll(diffModels_, m)
                {
                    Info<< nl << "    " << zName_ << "diffModels[" << m << "]" << nl;
                    Info<< "        zone size = "
                        << diffModels_[m]->cells().size() << nl;

                    if(diffModels_[m]->isFixed())
                    {
                        //Info<< "fixed: a m = " << a << "  " << m << "  " << nl;

                        diff_ = 0;
                        diffModels_[m]->evaluate();
                        //Info<< "hkr ->writeData() ";
                        diffModels_[m]->writeData();

                        // copy calculated zone diffusivity from Diff to diffSp[a]
                        forAll(diffModels_[m]->cells(), I)
                        {
                            label iCell = diffModels_[m]->cells()[I];
                            diffSpIn[iCell] = diff_[iCell];
                        }
                    }
                    else if(!diffModels_[m]->isBinary())
                    {
                         Info<< "ERROR: multicomponent diffusivity "
                             << "requires fixed or binary model"
    			     << exit(FatalError);
                    }
                    else
                    {
                        // species diffusivities in mixture from binary diffusivities
                        // ----------------------------------------------------------
                        // D_{a} = (1-x[a])/sum_{b!=a}(x[b]/D[a,b])
    
                        // pwise binary diff calculation and accumulation
                        // -------------------------------------------
                        // initialize sum(x[b]/D[ab]
                        scalarField sumA(diff_.size(), 0);
    
                        forAll(species_,b)
                        {
                            if (b != a)
                            {
                                diff_ = 0;
                                diffModels_[m]->setSpecies
                                (
                                    species_[a].name(),
                                    species_[b].name()
                                );
                                diffModels_[m]->evaluate();
                                diffModels_[m]->writeData();
    
                                forAll(diffModels_[m]->cells(), I)
                                {
                                    label iCell = diffModels_[m]->cells()[I];
                                    if(diff_[iCell] != 0)
                                    {
                                        sumA[iCell] += X_[b][iCell]/diff_[iCell];
                                    }
                                }
                            }
                        }

                        forAll(diffModels_[m]->cells(), I)
                        {
                            label iCell = diffModels_[m]->cells()[I];
                            if (sumA[iCell] != 0)
                            {
                                diffSpIn[iCell] = (1 - X_[a][iCell])/sumA[iCell];
                            }
                        }
                    } 
                }

                // account for lower diffusivity if pores are blocked by water
                diffSp_[a] *= pow(1. - s_, expBlockage_);

		Info<< "    ";
		check(diffSp_[a], SMALL, GREAT, false);
        	limit(SMALL, diffSp_[a], "Diffusivity"); // diffusivity should be larger 0
                diffSp_[a].correctBoundaryConditions();
            } 
        } 
    }
}


void Foam::materialDatabase::updateMoleFractions()
{
    sumYIonMI_ = 0.0;
    forAll(species_, i)
    {
        sumYIonMI_ += Y_[i]/species_[i].MW();
    }

    forAll(species_, i)
    {
        X_[i] = Y_[i]/species_[i].MW()/sumYIonMI_;
    }
}

void Foam::materialDatabase::limit
(
    const scalar min, 
    scalarField& field, 
    const word& name
)
{
    bool limited = false;
    forAll(field, i)
    {
        if(field[i] < min)
        {
            field[i] = min;
	    limited = true;
        }
    }

    if(limited)
    {
        Info<< "Warning: limited field " << name << endl;
    }
}

Foam::tmp<Foam::scalarField> 
Foam::materialDatabase::twoPhaseK(const dimensionedScalar& kWater)
{
    tmp<scalarField> pK2
    (
        new scalarField (k_.primitiveField())
    );
    scalarField& k2 = pK2.ref();
    const scalarField& sIn = s_.primitiveField();

    // gas
    k2 *= epsilon0().primitiveField()*(1.0 - sIn);

    // add porous media values
    forAll(porousZones(), iz)
    {
        label znId =
            mesh_.cellZones().findZoneID(porousZones()[iz].zoneName());
        labelList znCells(mesh_.cellZones()[znId]);

        dimensionedScalar kZn = porousZones()[iz].k();

        scalar porZn = porousZones()[iz].porosity();

        forAll(znCells, cellI)
        {
            k2[znCells[cellI]] += (1. - porZn)*kZn.value();
        }
    }
    // add liquid water
    k2 += epsilon0().primitiveField()*sIn*kWater.value();
    return pK2;
}

// ************************************************************************* //
