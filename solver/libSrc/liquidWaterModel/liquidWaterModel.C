/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::liquidWaterModel

Description
    Solves for transport of liquid water, and provides properties of the latter.

\*---------------------------------------------------------------------------*/

#include "liquidWaterModel.H"


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::liquidWaterModel::liquidWaterModel
(
    const IOdictionary& dict,
    materialDatabase& mat,
    patchDatabase& pm,
    phaseChange& pChange,
    pimpleControl& pimple
)
:
    pi_(constant::mathematical::pi),
    rho_("rho", dimDensity, dict.subDict("water")),
    k_("k", dimensionSet(1,1,-3,-1,0,0,0), dict.subDict("water")),
    cp_("cp", dimensionSet(0,2,-2,-1,0,0,0), dict.subDict("water")),
    dryChannel_(dict.subDict("water").lookupOrDefault<bool>("dryChannel", true)),
    pimple_(pimple),
    mat_(mat),
    pm_(pm),
    s_(mat.s()),
    T_(mat.T()),
    Svl_(pChange.Svl()),
    mu_
    (
        IOobject
        (
            "muWater",
            mat.mesh().time().timeName(),
            mat.mesh(),
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mat.mesh(),
        dimensionedScalar("mu", dimensionSet(1,-1,-1,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    Kgas_
    (
        IOobject
        (
            "Kgas",
            mat.mesh().time().timeName(),
            mat.mesh(),
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mat.mesh(),
        dimensionedScalar("0", dimensionSet(0,2,0,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    Kwater_
    (
        IOobject
        (
            "Kwater",
            mat.mesh().time().timeName(),
            mat.mesh(),
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mat.mesh(),
        dimensionedScalar("0", dimensionSet(0,2,0,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    J_
    (
        IOobject
        (
            "J",
            mat.mesh().time().timeName(),
            mat.mesh(),
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mat.mesh(),
        dimensionedScalar("0", dimensionSet(0,0,0,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    dJds_        
    (
        IOobject
        (
            "dJds",
            mat.mesh().time().timeName(),
            mat.mesh(),
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mat.mesh(),
        dimensionedScalar("0", dimensionSet(0,0,0,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    pc_        
    (
        IOobject
        (
            "pc",
            mat.mesh().time().timeName(),
            mat.mesh(),
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mat.mesh(),
        dimensionedScalar("0", dimensionSet(1,-1,-2,0,0,0,0), 0),
        zeroGradientFvPatchScalarField::typeName
    ),
    D_
    (
        IOobject
        (
            "Dw",
            mat.mesh().time().timeName(),
            mat.mesh(),
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mat.mesh(),
        dimensionedScalar("Dw", dimPressure*dimTime, 0)
    )
{
    if (dryChannel_)
    {
	Info << "Setting liquid water content in channels to zero." << endl;
    }
    updateMaterialProperties();
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void Foam::liquidWaterModel::solve()
{
    Info<< nl << "Solving for liquid water transport in "
	<< T_.mesh().name() << endl;

    // liquid water viscosity
    mu_ = dimensionedScalar("muW", dimensionSet(1,-1,-1,0,0,0,0), 2.414e-5)
	*pow(10, 247.8/(T_/dimensionedScalar("0", dimTemperature, 1.) - 140.));

    // interfacial drag coefficient
    volScalarField l("interfacial drag", Kwater_*mat_.mu() / (Kgas_*mu_));
    check(l, -GREAT, GREAT, false);

    // surface tension water-gas
    volScalarField sigma
    (
	(dimensionedScalar("0", dimensionSet(0,0,0,-1,0,0,0), -0.0001676)*T_ + 0.1218)
	*dimensionedScalar("0", dimensionSet(1,0,-2,0,0,0,0), 1.0)
    );

    // Leveret function, and its derivation by ds
    forAll(J_,i)
    {
        scalar s = s_[i];
        if (mat_.theta()[i] < pi_/2.0)    // 90°
        {
            J_[i] = 1.417*(1. - s) - 2.12*(1. - s)*(1. - s) + 1.263*(1. - s)*(1. - s)*(1. - s);
            dJds_[i] = -1.417 + 4.24*(1. - s) - 3.789*(1. - s)*(1. - s);
        }
        else
        {
            J_[i] = 1.417*s - 2.12*s*s + 1.263*s*s*s;
            dJds_[i] = 1.417 - 4.12*s + 3.789*s*s;
        }
    }

    // partial pressure
    pc_ = sigma*Foam::cos(mat_.theta())*Foam::sqrt(mat_.epsilon0()/mat_.K0())*J_;

    // derivation of partial pressure by ds
    volScalarField dpcds(sigma*Foam::cos(mat_.theta())*Foam::sqrt(mat_.epsilon0()/mat_.K0())*dJds_);

    // diffusion coefficient of liquid water
    // a diffusion coefficient of 0 leads to explosion
    D_ = -rho_
	*((Kwater_ + dimensionedScalar("0", dimensionSet(0,2,0,0,0,0,0), SMALL))/mu_)*dpcds;
    D_.correctBoundaryConditions();
    check(D_,-GREAT,GREAT,false);

    // boundary condition: grad s = -mass flux / (rho *D)
    // mass flux is already set in solveElectrochemistry

    // todo: changing sign of eqn leads to checkerboarding
    while (pimple_.correctNonOrthogonal())
    {
        fvScalarMatrix sEqn
        (
            fvm::ddt(rho_*mat_.epsilon0(), s_)
	    // todo: explosion due to large l at large s
	    // + fvc::div(matAir.epsilon0()*(1.-sAir)*rho*l*Uair,"div(water_drag)") 
          ==
            fvm::laplacian(D_,s_,"laplacian(Dw,s)")
	    // todo: moves in wrong direction and exploses
	    // - fvc::div(rhoW*KAirWater*(rhoW-matAir.rho())/muAirWater * g,"div(gravity)")
           + Svl_
        );

        sEqn.relax();
        sEqn.solve();
    }
    check(s_,0,1,false);

    // set water content in channels to 0, because water transport equation
    // has been derived with porous media equations
    if (dryChannel_)
    {    
        forAll(s_,i)
        {
            //if(s_[i] > sMax_){s_[i] = sMax;}  // with 1 it explodes
            //if(s_[i] < 0.0){s_[i] = 0.0;}
            if(mat_.porous()[i] == 0){s_[i] = 0.0;}
        }
    }

    s_.correctBoundaryConditions();
    updateMaterialProperties();
}


void Foam::liquidWaterModel::updateMaterialProperties()
{
    s_.correctBoundaryConditions(); // for correct KAir* at boundary

    mu_ = dimensionedScalar("muW", dimensionSet(1,-1,-1,0,0,0,0), 2.414e-5)
	*pow(10, 247.8/(T_/dimensionedScalar("0",dimTemperature, 1.) - 140.));

    // permeability corrected by water content
    Kgas_   = mat_.K0()*Foam::pow(1. - s_, 4);
    Kwater_ = mat_.K0()*Foam::pow(s_, 4);
}

// ************************************************************************* //
