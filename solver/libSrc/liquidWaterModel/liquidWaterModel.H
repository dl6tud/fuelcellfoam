/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::liquidWaterModel

Description
    Solves for transport of liquid water, and provides properties of the latter.

\*---------------------------------------------------------------------------*/

#ifndef liquidWaterModel_H
#define liquidWaterModel_H

#include "IOdictionary.H"
#include "materialDatabase.H"
#include "checkFieldLimits.H"
#include "patchDatabase.H"
#include "pimpleControl.H"
#include "phaseChange.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                    Class liquidWaterModel Declaration
\*---------------------------------------------------------------------------*/

class liquidWaterModel 
:
    private checkFieldLimits
{

private:

    // Private data

	const scalar pi_;
	//- Density
        const dimensionedScalar rho_;
	//- Thermal conductivity
        const dimensionedScalar k_;
	//- Isobaric heat capacity
        const dimensionedScalar cp_; 
	//- If true, the water content in the channels is set to zero
	const bool dryChannel_;

        pimpleControl& pimple_;
	//- Database for gas properties
	materialDatabase& mat_; 
	//- Database with patch names
        patchDatabase& pm_;
	//- Liquid water content
        volScalarField& s_; 
	//- Temperature
  	const volScalarField& T_;
	//- Phase change (source term vapour-liquid)
	const volScalarField& Svl_; 

	//- Viscosity
        volScalarField mu_;
	//- Permeability, corrected for gas phase
        volScalarField Kgas_;
	//- Permeability, corrected for liquid water
        volScalarField Kwater_;
	//- Leverett function
	volScalarField J_;
	//- Derivative of the Leverett function
	volScalarField dJds_;
	//- Capillary pressure
	volScalarField pc_; // capillary pressure
	//- "Diffusivity" of water for transport eqn
	volScalarField D_;


    // Private member functions

        //- Disallow copy construct
        liquidWaterModel(const liquidWaterModel&) = delete;

        //- Disallow default bitwise assignment
        void operator=(const liquidWaterModel&) = delete;


public:

    // Constructors

        liquidWaterModel
	(
	    const IOdictionary&, 
	    materialDatabase&, 
	    patchDatabase&, 
	    phaseChange&, 
	    pimpleControl&
	);


    // Destructor

        ~liquidWaterModel(){};


    // Member Functions

	//- Solve transport equation for liquid water
	void solve();

	//- Update the material properties
        void updateMaterialProperties();

	//- Viscosity
	const volScalarField& mu()
	{
	    return mu_;
	}

	//- Permeability (corrected for liquid water content)
        const volScalarField& Kwater()
        {
            return Kwater_;
	}

	//- Capillary pressure
        volScalarField& pc()
        {
            return pc_;
	}

	//- Isobaric heat capacity
	const dimensionedScalar cp()
	{
	    return cp_;
	}

	//- Density
        const dimensionedScalar rho()
	{
            return rho_;
	}

	//- Thermal conductivity
        const dimensionedScalar k()
        {
            return k_;
	}
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
