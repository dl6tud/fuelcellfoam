// stop simulation, if a 'stop' file there
if (isFile(abortName)) 
{
    Info<< "stopping simulation" << endl;
    runTime.writeAndEnd();
    return 0;
}

// write time step into file "time"
if (runTime.outputTime() and UPstream::myProcNo() == 0) 
{ 
    tf << runTime.timeName() << endl;
}

runTime.write();

Info<< "ExecutionTime = " << runTime.elapsedCpuTime() << " s"
    << "  ClockTime = " << runTime.elapsedClockTime() << " s"
    << "  SimulationTime = " << runTime.timeName()    << " s"
    << nl << endl;

// stop simulation, if runnting for 'calcTime' hours
if (calcTime)
{
    if (runTime.elapsedClockTime() > calcTime)
    {
        runTime.writeNow();
        return 0;
    }
}
