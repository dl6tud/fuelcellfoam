/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by the original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    fuelCellFoam

Description
    Steady solver for PEM fuel cells, using the unified flow theory
    for two-phase modelling

Contributors

    Original solver for solid oxide fuel cells
        Hrvoje Jasak (h.jasak@wikki.co.uk)
        Steven Beale (s.beale@fz-juelich.de)
        Dong Hyup Jeon (DongHyup.Jeon@nrc-cnrc.gc.ca)
        Helmut Roth (helmut.roth@nrc-cnrc.gc.ca)
        Hae-won Choi (haewon1972@gmail.com)

    Extension to PEM fuel cells
	Norbert Weber

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"

#include "patchToPatchInterpolation.H"
#include "continuityErrs.H"
#include "fixedGradientFvPatchFields.H"

#include "activationOverpotentialModel.H"
#include "ohmicOverpotentialModel.H"
#include "solidZoneList.H"

#include "pimpleControl.H"
#include "materialDatabase.H"
#include "checkFieldLimits.H"

#include "bTime.H"
#include "OFstream.H"
#include "patchDatabase.H"
#include "flowBalance.H"
#include "phaseChange.H"
#include "liquidWaterModel.H"
#include "electrolyteModel.H"
#include "mappingModel.H"
#include "electrochemistryModel.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    #include "setRootCase.H"
    #include "gitVersion.H" // output git version when running
    #include "createTime.H"

    // compute min, mean, max field value and warn if too low, or too high
    checkFieldLimits lim;

    #include "createMesh.H" // cell mesh
    #include "createControl.H"
    #include "readCellProperties.H"
    #include "createSolverParameters.H"
    #include "createMeshes.H" // child meshes

    #include "createCellFields.H"
    #include "createInterconnectFields.H"
    #include "createAirFields.H"
    #include "createElectrolyteFields.H"
    #include "createFuelFields.H"

    // names and IDs of different patches
    patchDatabase pm(mesh, airMesh, fuelMesh, electrolyteMesh);

    // ohmic overpotential
    autoPtr<ohmicOverpotentialModel> ohm
    (
        ohmicOverpotentialModel::New(electrolyteMesh, pm, electrolyteProperties.subDict("ohmicOverpotential"))
    );

    // properties and water transfer through electrolyte
    electrolyteModel elec(electrolyteMesh, matAir, matFuel, pm, ohm);

    // phase change models
    phaseChange phaseChangeAir(airProperties, matAir);
    phaseChange phaseChangeFuel(fuelProperties, matFuel);

    // liquid water properties
    liquidWaterModel airWater(cellProperties, matAir, pm, phaseChangeAir, pimple);
    liquidWaterModel fuelWater(cellProperties, matFuel, pm, phaseChangeFuel, pimple);

    // computes mass in- and outfluxes
    flowBalance airBalance(matAir, pm, airWater, phaseChangeAir);
    flowBalance fuelBalance(matFuel, pm, fuelWater, phaseChangeFuel);

    // mapping between parent and child meshes
    mappingModel map(mesh, airMesh, fuelMesh, electrolyteMesh, interconnectMesh, pm);

    // electrochemistry model
    electrochemistryModel electrochemistry
    (
        pm,
        matAir,
        matFuel,
        airBalance,
        fuelBalance,
        elec,
        ohm,
        airActivationOverpotential,
        fuelActivationOverpotential,
        map,
        cellProperties,
        Tcell
    );

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    for (runTime++; !runTime.end(); runTime++)
    {
        Info<< "Time = " << runTime.timeName() << endl;

	// get temperature to child meshes
	map.mapFromCell(Tcell, Tair, Tfuel, elec.T(), Tinterconnect);

	// update the gas properties
        #include "updateMaterials.H"

	// solve pressure and velocity
        #include "solveFuel.H"
        #include "solveAir.H"

	// compute Re numbers
	airBalance.calcRe();
	fuelBalance.calcRe();

	// solve for species
        #include "YfuelEqn.H"
        #include "YairEqn.H"

	// solver liquid water transport
        fuelWater.solve();
        airWater.solve();

	// solve phase change
        phaseChangeAir.solve();
        phaseChangeFuel.solve();

	// compute electrolyte water content
	elec.solveWaterContent();

	// solve electrochemistry
        electrochemistry.solve();

	// map properties to full cell, which are needed for energy eqn
        #include "mapToCell.H"

	// solve energy equation
        #include "solveEnergy.H"

	#include "write.H"
    }

    Info<< "End\n" << endl;
    return(0);
}


// ************************************************************************* //
