    Info<< nl << "Reading interconnect properties\n" << endl;
    IOdictionary interconnectProperties
    (
        IOobject
        (
            "interconnectProperties",
            runTime.constant(),
            interconnectMesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

    dimensionedScalar rhointerconnectGlobal("rho", dimDensity, interconnectProperties);
    dimensionedScalar CpinterconnectGlobal("Cp", dimensionSet(0,2,-2,-1,0,0,0) ,interconnectProperties);
    dimensionedScalar kinterconnectGlobal("k", dimensionSet(1,1,-3,-1,0,0,0), interconnectProperties);

    Info<< "    rhointerconnectGlobal = " << rhointerconnectGlobal << nl
        << "    CpinterconnectGlobal  = " << CpinterconnectGlobal << nl
        << "    kinterconnectGlobal   = " << kinterconnectGlobal << nl
        << endl;
    
    volScalarField kInterconnect
    (
        IOobject
        (
            "k",
            runTime.timeName(),
            interconnectMesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        interconnectMesh,
	kinterconnectGlobal,
        zeroGradientFvPatchScalarField::typeName
    );

    volScalarField cpInterconnect
    (
        IOobject
        (
            "cp",
            runTime.timeName(),
            interconnectMesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        interconnectMesh,
        CpinterconnectGlobal,
        zeroGradientFvPatchScalarField::typeName
    );

    volScalarField rhoInterconnect
    (
        IOobject
        (
            "rho",
            runTime.timeName(),
            interconnectMesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        interconnectMesh,
        rhointerconnectGlobal,
        zeroGradientFvPatchScalarField::typeName
    );

    Info<< "Creating interconnect zones" << endl;
    IOdictionary interconnectZones
    (
        IOobject
        (
            "interconnectZones",
            runTime.constant(),
            interconnectMesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );
    solidZoneList solidZones(interconnectMesh, interconnectZones);

    Info<< "    number of interconnect zones = " << solidZones.size() << nl;
    forAll(solidZones, i)
    {
        label iz = interconnectMesh.cellZones().findZoneID(solidZones[i].zoneName());
        Info<< "    zone " << solidZones[i].zoneIds()
            << " " << solidZones[i].zoneName()
            << " : size = " << interconnectMesh.cellZones()[iz].size()
            << "; k = " << solidZones[i].k().value() 
            << "; rho = " << solidZones[i].rho().value()
            << "; cp = " << solidZones[i].cp().value() << endl;
    }
    Info<< endl;

    forAll(solidZones, iz)
    {
        label znId =
            interconnectMesh.cellZones().findZoneID(solidZones[iz].zoneName());

        scalar kZn = solidZones[iz].k().value();
        scalar cpZn = solidZones[iz].cp().value();
        scalar rhoZn = solidZones[iz].rho().value();

        labelList znCells(interconnectMesh.cellZones()[znId]);

        forAll(znCells, cellI)
        {
            kInterconnect[znCells[cellI]] = kZn;
            cpInterconnect[znCells[cellI]] = cpZn;
            rhoInterconnect[znCells[cellI]] = rhoZn;
        }
    }
    kInterconnect.correctBoundaryConditions(); // for mapping boundaries
    cpInterconnect.correctBoundaryConditions();
    rhoInterconnect.correctBoundaryConditions();

    volScalarField Tinterconnect
    (
        IOobject
        (
            "T",
            runTime.timeName(),
            interconnectMesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        interconnectMesh,
        Tref,
        zeroGradientFvPatchScalarField::typeName
    );
