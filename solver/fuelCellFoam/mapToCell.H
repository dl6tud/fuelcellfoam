{
    Info<< "Mapping to cell" << endl;

    // clear the cell fields
    map.clear(rhoCpCell);
    map.clear(kCell);
    map.clear(TsourceCell);
    map.clear(phiCell);
    map.clear(eRhoCpPhiCell);
    map.clear(rhoCpGCell);
    map.clear(SvlCell);

    // map air to cell
    Tair.correctBoundaryConditions(); // for correct muW
    sAir.correctBoundaryConditions(); // for correct KAir* at boundary
    airWater.pc().correctBoundaryConditions(); // for correct rhoCpG
    matAir.cp().correctBoundaryConditions();   // for correct eRhoCpPhi at boundary
    matAir.rho().correctBoundaryConditions();

    map.air().rmap(rhoCpCell, matAir.cp()*matAir.rho());
    map.air().rmap(SvlCell, phaseChangeAir.Svl());
    scalarField airK(matAir.twoPhaseK(airWater.k()));
    map.air().rmap(kCell, airK);
    map.air().rmap(phiCell, phiAir);
    surfaceScalarField eRhoCpPhiAir
    (
        linearInterpolate((1. - sAir)*matAir.epsilon0()*matAir.cp())*phiAir
    );
    map.air().rmap(eRhoCpPhiCell,eRhoCpPhiAir);
    surfaceScalarField rhoCpGAir
    (
        airWater.rho()*airWater.cp()
	*fvc::interpolate(airWater.Kwater()/airWater.mu()) 
	*fvc::snGrad(airWater.pc())*airMesh.magSf()
    );
    map.air().rmap(rhoCpGCell,rhoCpGAir);

    // map fuel to cell
    Tfuel.correctBoundaryConditions(); // for correct muW
    sFuel.correctBoundaryConditions(); // for correct KFuel* at boundary
    fuelWater.pc().correctBoundaryConditions(); // for correct rhoCpG
    matFuel.cp().correctBoundaryConditions(); // for correct eRhoCpPhi at boundary
    matFuel.rho().correctBoundaryConditions();

    map.fuel().rmap(SvlCell, phaseChangeFuel.Svl());
    map.fuel().rmap(rhoCpCell, matFuel.cp()*matFuel.rho());
    scalarField fuelK(matFuel.twoPhaseK(fuelWater.k()));
    map.fuel().rmap(kCell, fuelK);
    map.fuel().rmap(phiCell, phiFuel);
    surfaceScalarField eRhoCpPhiFuel
    (
        linearInterpolate((1. - sFuel)*matFuel.epsilon0()*(matFuel.cp()))*phiFuel
    );
    map.fuel().rmap(eRhoCpPhiCell,eRhoCpPhiFuel);
    surfaceScalarField rhoCpGFuel
    (
	fuelWater.rho()*fuelWater.cp()
	*fvc::interpolate(fuelWater.Kwater()/fuelWater.mu())
	*fvc::snGrad(fuelWater.pc())*fuelMesh.magSf()
    );
    map.fuel().rmap(rhoCpGCell,rhoCpGFuel);

    // map electrolyte to cell
    map.electrolyte().rmap(rhoCpCell, elec.rho()*elec.cp());
    map.electrolyte().rmap(kCell, elec.k());
    map.electrolyte().rmap(TsourceCell, electrochemistry.electrochemicalHeating());
    // set all internal fluxes in the electrolyte to zero
    map.electrolyte().zeroInternalFlux(phiCell);
    map.electrolyte().zeroInternalFlux(eRhoCpPhiCell);
    map.electrolyte().zeroInternalFlux(rhoCpGCell);

    // map interconnect to cell
    map.interconnect().rmap(rhoCpCell, rhoInterconnect*cpInterconnect);
    map.interconnect().rmap(kCell, kInterconnect);

    // Correct boundary conditions
    rhoCpCell.correctBoundaryConditions();
    kCell.correctBoundaryConditions();

    // Check continuity of cell fluxes, as they appear in the energy equation
    continuityErrs(phiCell);
}
